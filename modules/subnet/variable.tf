variable "subnet_cidr" {
  type        = list(string)
  description = "List of all the public subnet cidr"
}



variable "subnet_tags" {
  type        = list(string)
  description = "List of all the public subnet tags"
}

variable "map_ip" {
  type        = list(bool)
  description = "status of public ip on instance launch"
}

variable "vpc_id" {
  type        = string
}


variable "az" {
  type        = list(string)
  description = "List of all the availibility zones"
} 