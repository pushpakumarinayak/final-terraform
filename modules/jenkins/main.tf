resource "aws_instance"  "ansible_public_ec2"{
 
  subnet_id                   = var.public_subnet_id
  ami                         = var.instance_ami1
  instance_type               = var.instance_type
  key_name                    = var.key_name
  iam_instance_profile        = "ec2_access"
  associate_public_ip_address = true
  security_groups    = [var.public_sg_id]
  
  tags = {
    Name = var.ansible_ec2_name
  }
   lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      security_groups,
    ]
  }  

}