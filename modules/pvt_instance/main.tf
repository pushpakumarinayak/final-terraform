resource "aws_instance" "private_ec2" {
  subnet_id              = var.private_subnet_id
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  security_groups        = [var.private_sg_id]
  key_name               = var.key_name
  
  
  
  tags = {
    Name = var.private_ec2_name
  }
}

resource "aws_instance" "private1_ec2" {
  subnet_id              = var.private_subnet_id
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  security_groups        = [var.private_sg_id]
  key_name               = var.key_name
  
  
  
  tags = {
    Name = var.private_ec2_name
  }
  }
  