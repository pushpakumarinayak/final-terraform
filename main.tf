provider "aws" {
  version = "4.22.0"
}

terraform {
  backend "s3" {
    bucket = "pushpa-terraform-bucket"
    key    = "testing-vpc/vpc1"
    region = "us-east-1"
  }
}

module "vpc_module" {
  source           = "./modules/vpc"
  cidr_block       = var.cidr_block
  vpc_name         = var.vpc_name
  instance_tenancy = var.instance_tenancy
}

module "subnet_module" {
  source      = "./modules/subnet"
  vpc_id      = module.vpc_module.vpc_id
  subnet_cidr = var.subnet_cidr
  subnet_tags = var.subnet_tags
  map_ip      = var.map_ip
  az          = var.az
}

module "internet_gateway_module" {
  source           = "./modules/internet_gateway"
  vpc_id           = module.vpc_module.vpc_id
  internet_gateway = var.internet_gateway
}
module "nat_gateway_module" {
  source           = "./modules/nat_gateway"
  publicsubnet     = module.subnet_module.public_subnet1
  var_depends_on   = "module.internet_gateway_module"
  nat_gateway_name = var.nat_gateway_name
}
module "route_table_module" {
  source             = "./modules/route_table"
  route_public_name  = var.route_public_name
  internet_gateway   = module.internet_gateway_module.aws_internet_gateway
  publicsubnet       = module.subnet_module.public_subnet1
  route_private_name = var.route_private_name
  privatesubnet      = module.subnet_module.private_subnet1
  nat_gateway        = module.nat_gateway_module.nat_gateway.id
  vpc_id             = module.vpc_module.vpc_id
}
module "security_group_module" {
  source                = "./modules/security_group"
  sg_publicname         = var.sg_publicname
  my_ip                 = var.my_ip
  sg_privatename        = var.sg_privatename
  vpc_id                = module.vpc_module.vpc_id
  public_ec2_private_ip = module.jenkins_module.public_ec2_private_ip
}
module "jenkins_module" {
  source            = "./modules/jenkins"
  instance_ami1      = var.instance_ami1
  instance_type     = var.instance_type
  key_name          = var.key_name
  ansible_ec2_name   = var.ansible_ec2_name
  public_subnet_id  = module.subnet_module.public_subnet1
  public_sg_id     = module.security_group_module.public_security_group_id
}

module "ec2_module" {
  source            = "./modules/instances"
  instance_ami      = var.instance_ami
  instance_type     = var.instance_type
  key_name          = var.key_name
  public_ec2_name   = var.public_ec2_name
  public_subnet_id  = module.subnet_module.public_subnet1
  public_sg_id      = module.security_group_module.public_security_group_id
}

module "ec2_module1" {
  source            = "./modules/pvt_instance"
  instance_ami      = var.instance_ami
  instance_type     = var.instance_type
  key_name          = var.key_name
  private_ec2_name  = var.private_ec2_name
  private_subnet_id = module.subnet_module.private_subnet1
  private_sg_id     = module.security_group_module.private_security_group_id
}