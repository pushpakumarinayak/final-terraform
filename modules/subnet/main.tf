resource "aws_subnet" "subnets" { # Creating Subnets
  count             = length(var.subnet_cidr)
  vpc_id            = var.vpc_id
  map_public_ip_on_launch  =  var.map_ip[count.index]
  cidr_block        = var.subnet_cidr[count.index] # CIDR block of subnets
  availability_zone = var.az[count.index]
  tags = {
    count = length(var.subnet_tags)
    Name  = var.subnet_tags[count.index]
  }

}