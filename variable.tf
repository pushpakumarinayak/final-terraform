variable "cidr_block" {
  type        = string
  description = "description"
}
variable "vpc_name" {
  type        = string
  description = "description"
}

variable "instance_tenancy" {
  type        = string
  description = "instance tencancy"
}

variable "subnet_cidr" {
  type        = list(string)
  description = "List of all the public subnet cidr"
}



variable "subnet_tags" {
  type        = list(string)
  description = "List of all the public subnet tags"
}

variable "map_ip" {
  type        = list(bool)
  description = "status of public ip on instance launch"
}


variable "az" {
  type        = list(string)
  description = "List of all the availibility zones"
}


variable "internet_gateway" {
  description = "internet gateway name"
  type        = string
}

variable "nat_gateway_name" {
  description = "nat gateway name"
  type        = string
}

variable "route_public_name" {
  description = "public route table name"
  type        = string
}

variable "route_private_name" {
  description = "private route table name"
  type        = string
}

variable "sg_publicname" {
  description = "public security group name"
  type        = string
}

variable "my_ip" {
  description = "Your IP address"
  type        = string
  sensitive   = true
}

variable "sg_privatename" {
  description = "private security group name"
  type        = string
}

variable "instance_ami" {
  type = string
}

variable "instance_ami1" {
  type = string
}

variable "ansible_ec2_name" {
  type        = string
  description = "name of ansible ec2 name"
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}


variable "public_ec2_name" {
  type        = string
  description = "name of public ec2 name"
}

variable "private_ec2_name" {
  type        = string
  description = "name of private ec2 name"
}