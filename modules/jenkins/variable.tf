variable "public_subnet_id" {
    type = string
    description = "public subnet id" 
}

variable "instance_ami1" {
  type    = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
    type = string
  
}

variable "ansible_ec2_name" {
  type        = string
  description = "name of ansible ec2 name"
}

variable "public_sg_id" {
    type = string
}